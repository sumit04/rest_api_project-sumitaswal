const express = require('express');
const rating = require('../models/rating');
const schema = require('../utils/validation');

const router = express.Router();

router.get('/:id', async (req, res, next) => {
  const request = schema.schemaId.validate(req.params);

  const result = await rating.readRating(req.params.id);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else if (result.length === 0 || result === false) {
    res.status(404).send('Id not found');
    next('Id not found');
  } else res.status(200).send(result);
});

router.get('/', async (req, res, next) => {
  const result = await rating.readAll();
  if (result.length === 0 || result === false) {
    res.status(400).send('Bad Request');
    next('Bad Request');
  } else res.status(200).send(result);
});

router.post('/', async (req, res, next) => {
  try {
    const request = schema.schemaRating.validate(req.body);
    const jsonValue = Object.values(req.body);
    const result = await rating.insertRating(jsonValue);
    if (request.error) {
      res.status(400).send(request.error.details[0].message);
      next(request.error.details[0].message);
    } else if (result === true) res.status(200).send('Successfuly inserted');
    else throw result;
  } catch (err) {
    res.status(400).send(err.message);
    next(err.message);
  }
});

router.delete('/:id', async (req, res, next) => {
  const request = schema.schemaId.validate(req.params);
  const result = await rating.deleteRating(req.params.id);
  if (request.error) {
    res.status(400).send(request.error.details[0].message);
    next(request.error.details[0].message);
  } else if (result.rowCount === 0 || result === false) {
    res.status(404).send('Id not found');
    next('Id not found');
  } else res.status(200).send('successfully deleted');
});

router.put('/:id', async (req, res, next) => {
  try {
    const requestBody = schema.schemaRating.validate(req.body);
    const requestId = schema.schemaId.validate(req.params);
    if (requestBody.error) {
      throw requestBody.error;
    }
    if (requestId.error) throw requestId.error;

    const valuesToUpdate = Object.values(req.body);
    valuesToUpdate.push(req.params.id);

    const result = await rating.updateRating(valuesToUpdate);
    if (result.rowCount === 0 || result === false) {
      res.status(404).send('Values not inserted properly');
      next('Values not inserted properly');
    } else res.status(201).send('updated');
  } catch (error) {
    res.status(400).send(error.details[0].message);
    next(error.details[0].message);
  }
});

module.exports = router;
