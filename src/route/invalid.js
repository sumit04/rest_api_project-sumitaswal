const express = require('express');

const router = express.Router();

router.get('*', function(req, res, next) {
  res.status(404).send('404 Not Found');
  next('404 Not Found');
});

router.post('*', function(req, res, next) {
  res.status(404).send('404 Not Found');
  next('404 Not Found');
});

router.put('*', function(req, res, next) {
  res.status(404).send('404 Not Found');
  next('404 Not Found');
});

router.delete('*', function(req, res, next) {
  res.status(404).send('404 Not Found');
  next('404 Not Found');
});

module.exports = router;
