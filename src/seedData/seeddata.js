const { Pool } = require('pg');
const Path = require('path');

// connecting to database through pooling

const pool = new Pool({
  user: 'postgres',
  password: '170426Mm!',
  host: 'localhost',
  port: 5432,
  database: 'postgres'
});

// query to create detailtable of all the player storing the details
const detailTable = () => {
  return pool.query(
    'CREATE TABLE playerDetails(ID INT PRIMARY KEY,Nation VARCHAR NOT NULL,Age INT NOT NULL,Club VARCHAR NOT NULL,Kit INT NOT NULL,Position VARCHAR NOT NULL)'
  );
};

// query to create rating table with all the rating of the players
const rating = () => {
  return pool.query(
    'CREATE TABLE playerRating(ID INT PRIMARY KEY,Name VARCHAR NOT NULL,Rating INT NOT NULL)'
  );
};

// query to insert values in table
const valueInsert = (tableName, path) => {
  return pool.query(
    // eslint-disable-next-line no-useless-escape
    `\COPY ${tableName} FROM ${path} with delimiter ',' csv header `
  );
};

// function which will call function to create and insert data in playerDetails table
const createTableDetail = () => {
  const tableName = 'playerDetails';
  const relativePath = Path.resolve('src/seedData', './playerDetail.csv');
  const path = `'${relativePath}'`;

  detailTable()
    .then(() => valueInsert(tableName, path))
    .then(() => console.log('successfuly created playerDetails table'))
    .catch(err => console.log(err));
};

// function which will call function to create and insert data in playerRating table
const createTableRating = () => {
  const tableName = 'playerRating';
  const relativePath = Path.resolve('src/seedData', './playerRating.csv');
  const path = `'${relativePath}'`;
  // const path = "'/home/sumit/js/api/src/seedData/playerRating.csv'";

  rating()
    .then(() => valueInsert(tableName, path))
    .then(() => console.log('successfuly created playerRating table'))
    .catch(err => console.log(err));
};

// query to drop the table
function dropTable() {
  return pool.query('DROP TABLE IF EXISTS playerRating,playerDetails ');
}

dropTable()
  .then(() => createTableRating())
  .then(() => createTableDetail())
  .catch(err => console.log(err));
