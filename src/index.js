const express = require('express');
const logger = require('./utils/logging');
const detail = require('./route/details');
const rating = require('./route/rating');
const invalid = require('./route/invalid');

const app = express();
app.use(logger.log);
app.use(express.json());

app.use('/detail', detail);
app.use('/rating', rating);

app.use('*', invalid);
app.use(logger.error);

const port = process.env.PORT || 8080;
app.listen(
  port,
  () =>
    function(error) {
      if (error) {
        console.log('error while starting server');
      } else {
        console.log(`hello sumit i am listing at ${port}`);
      }
    }
);
