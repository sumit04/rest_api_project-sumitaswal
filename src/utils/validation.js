const joi = require('@hapi/joi');

const schemaId = joi.object({
  id: joi
    .number()
    .integer()
    .required()
});

const schemaRating = joi.object({
  id: joi.number().integer(),
  name: joi
    .string()
    .min(3)
    .max(20)
    .required(),
  rating: joi
    .number()
    .integer()
    .max(100)
    .required()
});

const schemaDetail = joi.object({
  id: joi.number().integer(),
  nation: joi
    .number()
    .integer()
    .required(),
  age: joi
    .number()
    .integer()
    .min(18)
    .max(50)
    .required(),
  club: joi
    .string()
    .min(2)
    .max(20)
    .required(),
  kit: joi
    .number()
    .integer()
    .min(1)
    .max(3)
    .required(),
  position: joi
    .string()
    .min(2)
    .max(3)
    .required()
});

module.exports = { schemaId, schemaDetail, schemaRating };
