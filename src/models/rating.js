const { Pool } = require('pg');

const pool = new Pool({
  user: 'postgres',
  password: '170426Mm!',
  port: '5432',
  host: 'localhost',
  database: 'postgres'
});

// async function start() {
//   await pool.connect();
// }
// start();

async function readRating(id) {
  try {
    const sql = 'SELECT * FROM playerRating WHERE id=$1';
    const result = await pool.query(sql, [id]);
    return result.rows;
  } catch (error) {
    return error;
  }
}

async function readAll() {
  try {
    const sql = 'SELECT * FROM playerRating';
    const result = await pool.query(sql);
    return result.rows;
  } catch (error) {
    return error;
  }
}

async function insertRating(values) {
  try {
    const sql = 'INSERT INTO playerRating(ID,Name,Rating) VALUES($1,$2,$3)';
    await pool.query(sql, values);
    return true;
  } catch (err) {
    return err;
  }
}

async function deleteRating(id) {
  try {
    const sql = 'DELETE FROM playerRating where id=$1';
    return await pool.query(sql, [id]);
  } catch (error) {
    return false;
  }
}

async function updateRating(valuesToUpdate) {
  try {
    const sql = 'UPDATE playerRating SET name=$1,rating=$2 WHERE id=$3';
    return await pool.query(sql, valuesToUpdate);
  } catch (error) {
    return false;
  }
}
module.exports = {
  readRating,
  readAll,
  insertRating,
  deleteRating,
  updateRating
};
