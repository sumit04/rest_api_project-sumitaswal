const { Pool } = require('pg');
// const { dotenv } = require('dotenv');

// dotenv.config();

// const { password } = process.env;

const pool = new Pool({
  user: 'postgres',
  password: '170426Mm!',
  host: 'localhost',
  database: 'postgres',
  port: '5432'
});

// async function start() {
//   await pool.connect();
// }
// start();

async function readDetails(id) {
  try {
    const sql = 'SELECT * FROM playerDetails where id=$1';
    const result = await pool.query(sql, [id]);
    return result.rows;
  } catch (error) {
    return error;
  }
}

async function readAll() {
  try {
    const sql = 'SELECT * FROM playerDetails';
    const result = await pool.query(sql);
    return result.rows;
  } catch (error) {
    return error;
  }
}

async function insertDetails(values) {
  try {
    const sql =
      'INSERT INTO playerDetails(ID,Nation,Age,Club,Kit,Position) values($1,$2,$3,$4,$5,$6)';
    await pool.query(sql, values);
    return true;
  } catch (error) {
    return error;
  }
}

async function deleteDetails(id) {
  try {
    const sql = 'DELETE FROM playerDetails where id=$1';
    return await pool.query(sql, [id]);
  } catch (error) {
    return false;
  }
}

async function updateDetailsClub(ValuesToUpdate) {
  try {
    const sql =
      'UPDATE playerDetails SET Nation=$1,Age=$2,Club=$3,Kit=$4,Position=$5 WHERE id=$6';
    return await pool.query(sql, ValuesToUpdate);
  } catch (error) {
    return false;
  }
}

module.exports = {
  readDetails,
  readAll,
  insertDetails,
  deleteDetails,
  updateDetailsClub
};
