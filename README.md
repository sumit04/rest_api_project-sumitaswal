# RestFul API with EXPRESS

REST_API_Project.

Backend API for a web app.

It's a RESTful API that uses HTTP requests to GET, PUT, POST and DELETE data.
Its works on Express framework.


# Features
```
EXPRESS
```
```
REST API
```
```
Sequelize(ORM)
```
```
DATABASE - PostGres
```
```
Logging - Express-Winston
```
```
Validation - Hapi/Joi
```
```
Authentication - Jwt and bcrypt
```
# Important note

TO access all feature clone feat/ORM branch

```
In feat/ORM branch
-models are based on sequlelize ORM
-IT HAVE ALL FEATURE

Clone the branch feat/ORM using below command

git clone --single-branch  --branch feat/ORM git@gitlab.com:sumit04/rest_api_project-sumitaswal.git
```

```
In Master branch
-modles are written for Psotgress database
-there is no authentication feature

Clone master using below command

git clone git@gitlab.com:sumit04/rest_api_project-sumitaswal.git
```




## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

## Requirements and Prerequisites
```
node & npm
```
```
postgres
```
```
git
```

## Installing

To get the Node server running locally:
```
Clone the branch feat/ORM using below command

git clone --single-branch  --branch feat/ORM git@gitlab.com:sumit04/rest_api_project-sumitaswal.git
```
```
npm install to install all required dependencies
```
```
Install postgress database
```
```
FOR SEEDING- RUN npm run seedOrm
```
```
set .env variable for port and password of postgres
set .env for secretCode for JWT tokens
```
```
npm start to start the application
```
```
Postman
Install Postman to interact with REST API
```
```
Create a message with:
URL:

http://localhost:PORT/signUP
Method: POST
Body: raw + JSON (application/json)
Body Content: { "name":"name", "email": "email@eamil.com","password":"password" }

http://localhost:PORT/login
Method: POST
Body: raw + JSON (application/json)
Body Content: { "email": "email@eamil.com","password":"password" }

http://localhost:PORT/rating
Method: GET

http://localhost:PORT/detal
Method: GET
```

## Code Overview

#### Dependencies

```
 
    EXPRESS: The server for handling and routing HTTP requests
```
```
    POSTGRES: Database
```
```
    SEQUELIZE: It is a promise-based Node.js ORM
```
```
    JSONwebTOKEN and BcryptJS : For authentication and authorization.
```
```
    HAPI/JOI: FOR Validation
```
```
    Express-Winston: For logging the info and errors
```

#### Application Structure

```
src/index.js- The entry point to our application. [ npm start ]
```
```
src/seedData- It constains the script for seeding the data to database. [npm run seedOrm]
```
```
src/routes/- This folder contains the route for our API
```
```
src/models || src/ormModels- This folder contains the schema definitions for our models
```
```
src/controler-  This folder contains configuration  and defination for all routes of our APi
```

#### Error Handling

```
In src/middleware, we have define a error handling middleware
```

#### Authentication
```
Requests are authenticated using the Authorization header with a valid JWT.
We define express middlewares in src/middleware that can be used to authenticate requests.
```
    
   
## Deployment

```
Deployment can we done through many services present on net
```
```
You can use AWS EC2, its easy and free to deploy here with better services
```
```
step1- create a instance on AWS EC2(save the .pem as it is your private key)
```
```
step2-Use the chmod command (below) to make sure your private key file isn’t publicly viewable
      chmod 400 /path_to_key/my_key.pem
```
```
step3- SSH to your EC2 instance ( below)
       ssh -i /path_to_key/my_key.pem user_name@public_dns_name
```
```
step4- When inside your virtual environment

       Clone the branch feat/ORM using below command

       git clone --single-branch  --branch feat/ORM git@gitlab.com:sumit04/rest_api_project-sumitaswal.git
```
```
step5- npm install(for installing all the packages)
```
```
step6- install postgress database
```
```
step7- npm run seedOrm(for seeding)
```
```
step8- npm start(to start the app)
```
```
step9- after successfully connection you can intract with api through postman or your local browser
```

## Authors

* **Sumit Aswal**

